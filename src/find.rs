use crate::{static_re, LISTS_BASE_URL};

/* Normally numbers should be uxx, but they are only ever used in strings. */
#[derive(PartialEq, Debug)]
pub enum Find<'fk> {
	Bts(&'fk str),
	SalsaShort {
		project: &'fk str,
		subkind: &'fk str,
		id: &'fk str,
	},
	ListMessage(&'fk str),
	GitLab {
		base: &'fk str,
		project: &'fk str,
		subkind: &'fk str,
		id: &'fk str,
	},
	GitHub {
		project: &'fk str,
		subkind: &'fk str,
		id: &'fk str,
	},
}

impl Find<'_> {
	pub const fn names() -> &'static [&'static str] {
		&["bts", "salsa_short", "list", "gitlab", "github"]
	}

	pub fn name(&self) -> &'static str {
		match self {
			Self::Bts(..) => "bts",
			Self::SalsaShort {..} => "salsa_short",
			Self::ListMessage(..) => "list",
			Self::GitLab {..} => "gitlab",
			Self::GitHub {..} => "github",
		}
	}
}

pub const BTS_BASE_URL_RE: &str = r"bugs\.debian\.org/";
pub const BTS_LONG_URL_RE: &str = r"https://bugs\.debian\.org/cgi-bin/bugreport\.cgi\?bug=";

/* 1 - number
 * 
 * Here we assume the bug number has 3 or more digits, since the earliest
 * visible bug is #563.
 */
static_re!(RE_BTS_ID, r"(?:^|\s)#(\d{3,})\D?");
/* 1 - [[group/]project] + subkind, 3 - id */
static_re!(RE_SALSA_SHORT_ID, r"(?:^|\s)([\w/-]*(?:!|##)|[\w/-]+#)(\d+)");
/* 1 - number */
static_re!(RE_BTS_URL, &format!(r"(?:(?:^|[^\w.-]){BTS_BASE_URL_RE}|{BTS_LONG_URL_RE})(\d+)\D?"));
/* 1 - full URL (https://lists.debian.org/{list}/{year}/{month}/msg{id}.html) */
static_re!(RE_LIST_MESSAGE_URL, &format!(r"({LISTS_BASE_URL}/[\w-]+/\d+/\d+/msg\d+.html)"));
/* There are some restrictions on the domain name level, the (sub)group level,
 * and the project level, but a strict pattern would be too big to be worth it.
 * We don't anticipate a carefully crafted URL just to spoof us, without taking
 * the IRC server down first.
 * 
 * GitLab URLs have the signature `/-/` part, also _merge_requests_. OTOH,
 * obviously no one uses GitLab Enterprise for OSS projects.
 */
/* 1 - base URL, 2 - group and project, 3 - subkind, 4 - id */
static_re!(RE_GITLAB_URL, r"(https://[\w.-]+)/([\w/-]+)/-/(issues|merge_requests)/(\d+)");
/* 1 - account and repository, 2 - subkind, 3 - id */
static_re!(RE_GITHUB_URL, r"https://github\.com/([\w-]+/[\w.-]+)/(issues|pull)/(\d+)");

pub fn find_ids(message: &str) -> Vec<Find> {
	let mut results = Vec::new();

	for (_, cap) in RE_BTS_ID.captures_iter(message).map(|c| c.extract::<1>()) {
		results.push(Find::Bts(cap[0]));
	}

	for (_, cap) in RE_BTS_URL.captures_iter(message).map(|c| c.extract::<1>()) {
		results.push(Find::Bts(cap[0]));
	}

	for (_, cap) in RE_SALSA_SHORT_ID.captures_iter(message).map(|c| c.extract::<2>()) {
		let (project, subkind) = cap[0].split_at(cap[0].len() - 1);
		results.push(Find::SalsaShort {
			project: if project.ends_with('#') {
				""
			} else {
				project
			},
			subkind: match subkind {
				"!" => "merge_requests",
				"#" => "issues",
				_ => unreachable!("invalid salsa short id subkind")
			},
			id: cap[1]
		});
	}

	for (_, cap) in RE_LIST_MESSAGE_URL.captures_iter(message).map(|c| c.extract::<1>()) {
		results.push(Find::ListMessage(cap[0]));
	}

	for (_, cap) in RE_GITLAB_URL.captures_iter(message).map(|c| c.extract::<4>()) {
		results.push(Find::GitLab {
			base: cap[0],
			project: cap[1],
			subkind: cap[2],
			id: cap[3]
		});
	}

	for (_, cap) in RE_GITHUB_URL.captures_iter(message).map(|c| c.extract::<3>()) {
		results.push(Find::GitHub {
			project: cap[0],
			subkind: cap[1],
			id: cap[2]
		});
	}

	results
}

#[cfg(test)]
mod test {
	use crate::*;
	use super::*;

	#[test]
	fn find_bugs() {
		assert_eq!(find_ids("#1000000 something #123456"), [
			Find::Bts("1000000"),
			Find::Bts("123456"),
		]);
		assert_eq!(find_ids(&format!("look at {BTS_BASE_URL}/1000000?")), [Find::Bts("1000000")]);
	}

	#[test]
	fn find_salsa_short() {
		assert_eq!(find_ids("check out debian/foo!1."), [Find::SalsaShort { project: "debian/foo", subkind: "merge_requests", id: "1" }]);
		assert_eq!(find_ids("see debcargo-conf!123!"), [Find::SalsaShort { project: "debcargo-conf", subkind: "merge_requests", id: "123" }]);
		assert_eq!(find_ids("!123!"), [Find::SalsaShort { project: "", subkind: "merge_requests", id: "123" }]);
		assert_eq!(find_ids(" debian/foo#1?"), [Find::SalsaShort { project: "debian/foo", subkind: "issues", id: "1" }]);
		assert_eq!(find_ids("debcargo-conf#123."), [Find::SalsaShort { project: "debcargo-conf", subkind: "issues", id: "123" }]);
		assert_eq!(find_ids("##123."), [Find::SalsaShort { project: "", subkind: "issues", id: "123" }]);
	}

	#[test]
	fn find_list_messages() {
		let url = "https://lists.debian.org/debian-devel/2024/01/msg00212.html";
		assert_eq!(find_ids(&format!("here's the mail:{url}")), [Find::ListMessage(url)]);
	}

	#[test]
	fn find_gitlab() {
		assert_eq!(find_ids(&format!("at {SALSA_BASE_URL}/rust-team/debcargo/-/merge_requests/1…")), [
			Find::GitLab { base: SALSA_BASE_URL, project: "rust-team/debcargo", subkind: "merge_requests", id: "1" }
		]);
		assert_eq!(find_ids(&format!("!{SALSA_BASE_URL}/rust-team/debcargo/-/issues/1!")), [
			Find::GitLab { base: SALSA_BASE_URL, project: "rust-team/debcargo", subkind: "issues", id: "1" }
		]);
		assert_eq!(find_ids("!https://gitlab.com/gitlab-org/imaginarysubgroup/gitlab/-/issues/1!"), [
			Find::GitLab { base: "https://gitlab.com", project: "gitlab-org/imaginarysubgroup/gitlab", subkind: "issues", id: "1" }
		]);
	}

	#[test]
	fn find_github() {
		assert_eq!(find_ids("check this out:https://github.com/rust-lang/rust/issues/12345#comment-1111111"), [
			Find::GitHub { project: "rust-lang/rust", subkind: "issues", id: "12345" }
		]);
		assert_eq!(find_ids("check this out-https://github.com/rust-lang/rust/pull/12345;"), [
			Find::GitHub { project: "rust-lang/rust", subkind: "pull", id: "12345" }
		]);
	}

	#[test]
	fn find_complex() {
		let find = find_ids("some bugs, MRs, and issues: #999999, https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=77777, rust!12, https://bugs.debian.org/888888, debcargo#34, ci-team/autopkgtest!23, https://salsa.debian.org/rust-team/debcargo-conf/-/merge_requests/45..");
		assert_eq!(find, [
			Find::Bts("999999"),
			Find::Bts("77777"),
			Find::Bts("888888"),
			Find::SalsaShort { project: "rust", subkind: "merge_requests", id: "12" },
			Find::SalsaShort { project: "debcargo", subkind: "issues", id: "34" },
			Find::SalsaShort { project: "ci-team/autopkgtest", subkind: "merge_requests", id: "23" },
			Find::GitLab { base: SALSA_BASE_URL, project: "rust-team/debcargo-conf", subkind: "merge_requests", id: "45" },
		]);
	}
}
