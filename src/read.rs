use {
	html_escape::decode_html_entities as unescape,
	anyhow::{Result, Context},
	crate::{
		find::Find,
		salsa_short_to_full,
		static_re,
		BTS_BASE_URL, BTS_LONG_URL, SALSA_BASE_URL,
	},
};

static_re!(RE_PAGE_TITLE, r"<title>(.*?)</title>");

async fn get<T: reqwest::IntoUrl>(url: T) -> reqwest::Result<reqwest::Response> {
	reqwest::ClientBuilder::default()
		.user_agent(format!("fe2o3bot/{}", env!("CARGO_PKG_VERSION")))
		.build()?
		.get(url)
		.send()
		.await
}

impl Find<'_> {
	pub async fn read(&self) -> Result<String> {
		match self {
			Self::Bts(number) => bts(number).await,
			Self::ListMessage(url) => list_message(url).await,
			Self::SalsaShort { project, subkind, id } => gitlab(SALSA_BASE_URL, &salsa_short_to_full(project), subkind, id).await,
			Self::GitLab { base, project, subkind, id } => gitlab(base, project, subkind, id).await,
			Self::GitHub { project, subkind, id } => github(project, subkind, id).await,
		}
	}

	pub fn canonical_url(&self) -> String {
		match self {
			Self::Bts(number) => format!("{BTS_BASE_URL}/{number}"),
			Self::SalsaShort { project, subkind, id } => format!(
				"{SALSA_BASE_URL}/{}/-/{}/{id}",
				salsa_short_to_full(project),
				subkind,
			),
			Self::ListMessage(url) => url.to_string(),
			Self::GitLab { base, project, subkind, id } => format!("{base}/{project}/-/{subkind}/{id}"),
			Self::GitHub { project, subkind, id } => format!("https://github.com/{project}/{subkind}/{id}"),
		}
	}
}
async fn bts(number: &str) -> Result<String> {
	let page = get(format!("{BTS_LONG_URL}{number}")).await
		.context("failed to retrieve BTS bug")?
		.text().await
		.context("failed to get response text for BTS bug")?;

	let title = RE_PAGE_TITLE.captures(&page)
		.context("no title matched on BTS bug page")?
		.get(1).unwrap().as_str();
	let title = unescape(title);
	
	Ok(format!("\"{title}\""))
}

async fn list_message(url: &str) -> Result<String> {
	let page = get(url).await
		.context("failed to retrieve list message")?
		.text().await
		.context("failed to read list message")?;

	let title = RE_PAGE_TITLE.captures(&page)
		.context("no title match on list message page")?
		.get(1).unwrap().as_str();
	let title = unescape(title);

	Ok(format!("\"{title}\""))
}

#[derive(serde::Deserialize)]
struct GitLabItem {
	title: String,
	state: String,
}

async fn gitlab(base: &str, project: &str, subkind: &str, id: &str) -> Result<String> {
	let project = project.replace('/', "%2F");
	let api_url = format!("{base}/api/v4/projects/{project}/{subkind}/{id}");
	let GitLabItem { title, state } = get(api_url).await
		.map_err(|e| {
			let context = format!("failed to retrieve gitlab data: {e:?}");
			anyhow::Error::new(e).context(context)
		})?
		.json()
		.await
		.map_err(|e| {
			let context = format!("failed to deserialize gitlab data as json: {e:?}");
			anyhow::Error::new(e).context(context)
		})?;
	Ok(format!("\"{title}\" ({state})"))
}

#[derive(serde::Deserialize)]
struct GitHubItem {
	title: String,
	state: String,
}

async fn github(project: &str, subkind: &str, id: &str) -> Result<String> {
	let subkind = subkind.replace("pull", "pulls");
	let api_url = format!("https://api.github.com/repos/{project}/{subkind}/{id}");
	let GitHubItem { title, state } = get(&api_url).await
		.map_err(|e| {
			let context = format!("failed to retrieve github data: {e:?}");
			anyhow::Error::new(e).context(context)
		})?
		.json()
		.await
		.map_err(|e| {
			let context = format!("failed to deserialize github data as json: {e:?}");
			anyhow::Error::new(e).context(context)
		})?;
	Ok(format!("\"{title}\" ({state})"))
}
