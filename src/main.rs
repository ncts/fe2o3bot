mod find;
mod read;

use {
	irc::{
		client::{Client, data::Config},
		proto::{Command, Prefix},
	},
	futures::StreamExt,
	tracing::{info, warn},
	anyhow::Result,
	crate::find::{Find, find_ids, RE_SALSA_SHORT_ID},
};

const CONFIG: &str = "config.toml";

pub const BTS_BASE_URL: &str = "https://bugs.debian.org";
pub const BTS_LONG_URL: &str = "https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=";
pub const SALSA_BASE_URL: &str = "https://salsa.debian.org";
pub const LISTS_BASE_URL: &str = "https://lists.debian.org";

fn salsa_short_to_full(project: &str) -> String {
	match project {
		"" => "rust-team/debcargo-conf".to_string(),
		project if project.contains('/') => project.to_string(),
		project => format!("rust-team/{project}")
	}
}

#[macro_export]
macro_rules! static_re {
	($name:ident, $pattern:expr) => {
		pub static $name: once_cell::sync::Lazy<regex::Regex> = once_cell::sync::Lazy::new(||
			regex::Regex::new($pattern)
			.expect(&format!("failed to build Regex {}", stringify!($name))));
	};
}

fn send(client: &Client, channel: &str, message: &str) -> Result<()> {
	client.send_notice(channel, message)
		.map_err(|e| {
			let context = format!("irc send failed: {e:?}");
			anyhow::Error::new(e).context(context)
		})
}

#[tokio::main]
async fn main() -> Result<()> {
	let tracing_sub = tracing_subscriber::FmtSubscriber::new();
	tracing::subscriber::set_global_default(tracing_sub)
		.expect("tracing failed to set default subscriber");

	let config = Config::load(CONFIG).expect("failed to load config");
	info!(nickname = config.nickname, server = config.server, "configure bot");
	let owners = config.owners.clone();

	let kind_names = Find::names();
	let mut disabled: Vec<String> = config.options.get("disabled")
		.map(|raw| raw.split(',').filter_map(|v| if kind_names.contains(&v) {
			Some(v.to_string())
		} else {
			warn!(finder = v, "configured disabled finder not found");
			None
		}).collect())
		.unwrap_or_default();

	let mut client = Client::from_config(config.clone()).await?;
	client.identify()?;
	let mut stream = client.stream()?;

	while let Some(message) = stream.next().await.transpose()? {
		let nick = if let Some(Prefix::Nickname(nick, _, _)) = message.prefix {
			// skip own messages
			if nick == client.current_nickname() {
				continue
			}
			nick
		} else {
			"".to_string()
		};

		if let Command::PRIVMSG(channel, message) = message.command {
			if message.starts_with('!') && !RE_SALSA_SHORT_ID.is_match(&message) {
				let parts: Vec<_> = message.splitn(2, ' ').map(|s| s.to_string()).collect();
				let command = &parts[0];
				let args = parts.get(1);
				info!(command = command, args = args, commander = nick, "command");
				let result = match command.as_str() {
					"!disable-finder" | "!enable-finder" => {
						if !owners.contains(&nick) {
							send(&client, &channel, "You are not in owners")
						} else if let Some(arg) = args {
							if kind_names.contains(&arg.as_str()) {
								let idx = disabled.iter().position(|x| x == arg);
								if command == "!disable-finder" && idx.is_none() {
									disabled.push(arg.to_string());
								} else if command == "!enable-finder" && idx.is_some() {
									disabled.remove(idx.unwrap());
								}
								Ok(())
							} else {
								send(&client, &channel, &format!("Unknown finder to disable/enable: {arg}"))
							}
						} else {
							send(&client, &channel, "Nothing to disable/enable")
						}
					},
					"!finders" => {
						let enabled = kind_names.iter()
							.filter_map(|k| if disabled.iter().any(|v| v.as_str() == *k) { None } else { Some(*k) })
							.collect::<Vec<_>>().join(", ");
						let disabled = disabled.iter()
							.map(|v| v.as_ref())
							.collect::<Vec<_>>().join(", ");
						send(&client, &channel, &format!("Currently enabled: {enabled}; disabled: {disabled}"))
					}
					"!owners" => send(&client, &channel, &format!("Owners: `{}`", owners.join("`, `"))),
					_ => send(&client, &channel, "Unknown command"),
				};

				if let Err(e) = result {
					warn!(error = e.to_string(), command = command, "command reply error");
				}

				continue
			}

			for result in find_ids(&message) {
				if disabled.iter().any(|v| v.as_str() == result.name()) {
					continue
				}
				
				match result.read().await {
					Ok(read) => if let Err(e) = send(&client, &channel, &format!("{read} - {}", result.canonical_url())) {
						warn!(error = e.to_string(), "send error");
					},
					Err(e) => warn!(error = e.to_string(), source = format!("{result:?}"), "retrieving data")
				}
			}
		}
	}

	Ok(())
}
