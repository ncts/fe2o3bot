FROM docker.io/library/rust as build
WORKDIR /build
COPY Cargo.* /build/
COPY src /build/src
RUN cargo build --release

FROM docker.io/library/debian:unstable-slim
COPY --from=build /build/target/release/fe2o3bot /bin/fe2o3bot
WORKDIR /work
CMD ["/bin/fe2o3bot"]
